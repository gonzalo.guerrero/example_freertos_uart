//
// Created by akapustin on 13.02.2021.
//

#include <stdint.h>
#include <stdbool.h>

#include "stm32l1xx.h"

#include "FreeRTOS.h"
#include "queue.h"
#include "semphr.h"

#include "uart.h"

#define UART_IN_BUFFER_SIZE     128
#define UART_OUT_BUFFER_SIZE    128

static struct {
    StaticQueue_t in_queue;
    uint8_t in_queue_buffer[UART_IN_BUFFER_SIZE];
    QueueHandle_t in_queue_handle;

    StaticQueue_t out_queue;
    uint8_t out_queue_buffer[UART_OUT_BUFFER_SIZE];
    QueueHandle_t out_queue_handle;

    bool in_error;

    uint8_t rx_byte, tx_byte;

    UART_HandleTypeDef *huart;

    SemaphoreHandle_t send_mtx;
    StaticSemaphore_t send_mtx_buffer;
} uart_fields;


static void uart_rx_cmplt_callback(UART_HandleTypeDef *huart);

static void uart_tx_callback(UART_HandleTypeDef *huart);

static void uart_error_callback(UART_HandleTypeDef *huart);

static struct uart_cb_record {
    uint16_t id;
    void (* cb)(UART_HandleTypeDef *hcan);
} uart_cb_records[] = {
    { HAL_UART_TX_COMPLETE_CB_ID, uart_tx_callback },
    { HAL_UART_RX_COMPLETE_CB_ID, uart_rx_cmplt_callback },
    { HAL_UART_ERROR_CB_ID, uart_error_callback },
};

void uart_init(UART_HandleTypeDef *huart) {
    uart_fields.huart = huart;

    uart_fields.in_queue_handle = xQueueCreateStatic(UART_IN_BUFFER_SIZE, 1, uart_fields.in_queue_buffer, &uart_fields.in_queue);
    uart_fields.out_queue_handle = xQueueCreateStatic(UART_OUT_BUFFER_SIZE, 1, uart_fields.out_queue_buffer, &uart_fields.out_queue);

    uart_fields.send_mtx = xSemaphoreCreateMutexStatic(&uart_fields.send_mtx_buffer);

    int record_count = sizeof(uart_cb_records) / sizeof(uart_cb_records[0]);
    for (size_t i = 0; i < record_count; i++) {
        if (HAL_UART_RegisterCallback(uart_fields.huart, uart_cb_records[i].id, uart_cb_records[i].cb) != HAL_OK) {
            uart_fields.in_error = true;
            return;
        }
    }

    uart_fields.in_error = HAL_UART_Receive_IT(uart_fields.huart, &uart_fields.rx_byte, 1) != HAL_OK;
}

void uart_rx_cmplt_callback(UART_HandleTypeDef *huart) {
    uint8_t byte = uart_fields.rx_byte;

    xQueueSendFromISR(uart_fields.in_queue_handle, &byte, NULL);
    HAL_UART_Receive_IT(uart_fields.huart, &uart_fields.rx_byte, 1);
}

void uart_tx_callback(UART_HandleTypeDef *huart) {
    if (uart_fields.in_error)
        return;

    if (xQueueReceiveFromISR(uart_fields.out_queue_handle, &uart_fields.tx_byte, NULL) == pdPASS) {
        if (HAL_UART_Transmit_IT(uart_fields.huart, (uint8_t *) &uart_fields.tx_byte, 1) == HAL_OK) {
            uart_fields.in_error = false;
        } else {
            uart_fields.in_error = true;
        }
    }
}

void uart_error_callback(UART_HandleTypeDef *huart) {
    uart_fields.in_error = true;
}

bool uart_send(const uint8_t *data, size_t len) {
    if (len == 0) {
        return false;
    }

    if(xSemaphoreTake(uart_fields.send_mtx, portMAX_DELAY) != pdTRUE) {
        return false;
    }

    bool should_start_tx = uxQueueMessagesWaiting(uart_fields.out_queue_handle) == 0;
    for (size_t i = 0; i < len; i++) {
        xQueueSend(uart_fields.out_queue_handle, &data[i], portMAX_DELAY);
    }

    xSemaphoreGive(uart_fields.send_mtx);

    if (should_start_tx) {
        if (xQueueReceive(uart_fields.out_queue_handle, &uart_fields.tx_byte, portMAX_DELAY) == pdPASS) {
            if (HAL_UART_Transmit_IT(uart_fields.huart, (uint8_t *) &uart_fields.tx_byte, 1) == HAL_OK) {
                uart_fields.in_error = false;
            } else {
                uart_fields.in_error = true;
            }
        }
    }

    return true;
}

bool uart_read_byte(uint8_t *byte, unsigned int timeout) {
    return xQueueReceive(uart_fields.in_queue_handle, byte, pdMS_TO_TICKS(timeout)) == pdPASS;
}
