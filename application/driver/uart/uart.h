//
// Created by akapustin on 13.02.2021.
//

#ifndef EXAMPLE_UART_UART_H
#define EXAMPLE_UART_UART_H

#include <stdbool.h>

#include <stm32l1xx_hal.h>

void uart_init(UART_HandleTypeDef *huart);
bool uart_send(const uint8_t *data, size_t len);
bool uart_read_byte(uint8_t *byte, unsigned int timeout);

#endif //EXAMPLE_UART_UART_H
