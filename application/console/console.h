//
// Created by akapustin on 13.02.2021.
//

#ifndef EXAMPLE_UART_CONSOLE_H
#define EXAMPLE_UART_CONSOLE_H

void console_init();
void console_loop();
void console_prompt();

#endif //EXAMPLE_UART_CONSOLE_H
