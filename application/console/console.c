//
// Created by akapustin on 13.02.2021.
//

#include <stdint.h>
#include <string.h>
#include <stdio.h>

#include "FreeRTOS.h"

#include "console.h"
#include "uart.h"

#define CONSOLE_MAX_CMD_LEN     10
#define CONSOLE_MAX_STR_LEN     255

struct {
    enum {
        console_state_prompt,
        console_state_awaiting_command,
        console_state_command_ready
    } state;

    char cmd[CONSOLE_MAX_CMD_LEN];
    char *cmd_p;
} console_fields;

static void console_hello();

static void console_whoami();

static void console_say(const char *msg);

static struct {
    const char *cmd;
    void (*cb)();
} console_cb_records[] = {
        { "hello", console_hello },
        { "whoami", console_whoami }
};

void console_init() {
    console_fields.state = console_state_prompt;
}

void console_loop() {
    switch (console_fields.state) {
        case console_state_prompt: {
            memset(console_fields.cmd, 0, sizeof(console_fields.cmd));
            console_fields.cmd_p = console_fields.cmd;
            console_prompt();
            console_fields.state = console_state_awaiting_command;
            break;
        }
        case console_state_awaiting_command: {
            uint8_t byte;
            if (!uart_read_byte(&byte, 1000)) {
                break;
            }

            *console_fields.cmd_p = byte;
            uart_send(console_fields.cmd_p, 1);

            if (*console_fields.cmd_p == 13) {
                console_say("\r");
                *console_fields.cmd_p = 0;
                console_fields.state = console_state_command_ready;
            } else {
                console_fields.cmd_p++;
            }
            break;
        }
        case console_state_command_ready: {
            size_t record_count = sizeof(console_cb_records) / sizeof(console_cb_records[0]);
            for (int i = 0; i < record_count; i++) {
                if (strncmp(console_cb_records[i].cmd, console_fields.cmd, CONSOLE_MAX_CMD_LEN) == 0) {
                    console_cb_records[i].cb();
                }
            }

            console_fields.state = console_state_prompt;
            break;
        }
    }
}

void console_prompt() {
    static int count = 0;
    uint8_t spin[] = "|/-\\";
    char msg[CONSOLE_MAX_STR_LEN];
    snprintf(msg, CONSOLE_MAX_STR_LEN, "\r\033[K %c > %s", spin[(count++) % 4], console_fields.cmd);
    console_say(msg);
}

void console_say(const char *msg) {
    size_t len = strnlen(msg, CONSOLE_MAX_STR_LEN);
    uart_send(msg, len);
}

void console_hello() {
    console_say("\r\n\nHello!\r\n");
}

void console_whoami() {
    console_say("\r\n\nI'm stm32l152ret6\r\n");
}
